#include"sniffer.h"
#include"PTree.h"
#include"daemon.h"
#include<stdio.h> //For standard things
#include<stdlib.h>    //malloc
#include<string.h>    //memset
#include<netinet/ip_icmp.h>   //Provides declarations for icmp header
#include<netinet/udp.h>   //Provides declarations for udp header
#include<netinet/tcp.h>   //Provides declarations for tcp header
#include<netinet/ip.h>    //Provides declarations for ip header
#include<sys/socket.h>
#include<arpa/inet.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<linux/fs.h>
#include<sys/signal.h>
#if !defined(_GNU_SOURCE)
    #define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <execinfo.h>
#include <unistd.h>
#include <errno.h>
#include <wait.h>
#include <sys/time.h>
#include <sys/resource.h>
#define FD_LIMIT			1024*10
#define CHILD_NEED_WORK			1
#define CHILD_NEED_TERMINATE	2
#define PID_FILE "/var/run/my_daemon.pid"
#define LOG_FILE "/var/log/my_daemon.log"
#define CNF_FILE "/etc/my_daemon.conf"
#define DAT_FILE "/var/lib/my_daemon.dat"
#include<pthread.h>
#define DELIM "="
#include <string.h>
#include<signal.h>
#include <sys/types.h>
#include <ifaddrs.h>

PTree* readTree;
pthread_t thread;
int NumOfIface = 0;
int NumOfCur = 0;
int sock_raw;
struct sockaddr_in source,dest;

int main(int argc, char** argv)
{
    readTree=NULL;
    int status;
    int pid;
    char interfaceName[256];
    NumOfIface = 0;
    struct ifaddrs *addrs,*tmp;

    getifaddrs(&addrs);
    if (addrs->ifa_addr && addrs->ifa_addr->sa_family == AF_PACKET){
        memset(interfaceName, '\0', sizeof(interfaceName));
        strncpy(interfaceName, addrs->ifa_name,sizeof(interfaceName)- 1);
    }
    tmp = addrs;

    while (tmp)
    {
        if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_PACKET)
            NumOfIface++;

        tmp = tmp->ifa_next;
    }

    freeifaddrs(addrs);
    if (argc < 2)
    {
        printf("Usage: ./my_daemon [command]\n Write ./my_daemon --help fow more info");
        return -1;
    }
    else if(strcmp(argv[1],"start") == 0)
    {
        FILE* file = fopen(CNF_FILE,"w");
        if(file!=NULL)
            fprintf(file,"%d %s\n",NumOfCur,interfaceName);
        fclose(file);
        pid = fork();
        if (pid == -1)
        {
            printf("Start Daemon Error: %s\n", strerror(errno));
            return -1;
        }
        else if (!pid)
        {
            printf("Start Daemon\n");
            umask(0);
            setsid();
            chdir("/");
            close(STDIN_FILENO);
            close(STDOUT_FILENO);
            close(STDERR_FILENO);
            SetPidFile(PID_FILE);
            status = MonitorProc(interfaceName);
            return status;
        }
        else
            return 0;
    }
    else if(strcmp(argv[1],"stop") == 0)
    {
        sendReload(SIGTERM);
        return 0;
    }
    else if(strcmp(argv[1],"show") == 0 && argc >= 3)
    {
        sendReload(SIGUSR2);
        FILE* file = fopen(DAT_FILE,"r");
        if (file == NULL)
        {
            printf("No statistics\n");
            return -1;
        }
        readBinaryTree(&readTree,file);
        fclose(file);
        uint32_t key;
        inet_pton(AF_INET,argv[2],&key);
        file = fopen(CNF_FILE,"r");
        char buf[256];
        fscanf(file, "%d %s\n", &NumOfCur, buf);
        fclose(file);
        PTree* x = search(key,readTree);
        if (x != 0)
            printf("%lld\n",x->num[NumOfCur]);
        destroy_tree(readTree);
    }
    else if(strcmp(argv[1],"stat") == 0  && argc >=3)
    {
        sendReload(SIGUSR2);
        int newN=0;
        getifaddrs(&addrs);
        tmp = addrs;
        while (tmp)
        {
            if (strcmp(argv[2],tmp->ifa_name) == 0)
            {
                NumOfCur = newN;
                break;
            }
            newN++;
            tmp = tmp->ifa_next;
        }

        freeifaddrs(addrs);
        FILE* file = fopen(DAT_FILE,"r");
        if (file == NULL)
        {
            printf("No statistics\n");
            return -1;
        }
        while(readTree == 0)
            readBinaryTree(&readTree,file);
        fclose(file);
        printBinaryTree(readTree,NumOfCur);
        destroy_tree(readTree);
    }
    else if(strcmp(argv[1],"select") == 0 && argc >=3)
    {
        int newN=0;
        getifaddrs(&addrs);
        if (addrs->ifa_addr && addrs->ifa_addr->sa_family == AF_PACKET){
            memset(interfaceName, '\0', sizeof(interfaceName));
            strncpy(interfaceName, addrs->ifa_name,sizeof(interfaceName)- 1);

        }
        tmp = addrs;
        while (tmp)
        {
            if (strcmp(argv[2],tmp->ifa_name) == 0)
            {
                NumOfCur = newN;
                memset(interfaceName, '\0', sizeof(interfaceName));
                strncpy(interfaceName, argv[2],sizeof(interfaceName)- 1);
                break;


            }
            newN++;
            tmp = tmp->ifa_next;
        }

        freeifaddrs(addrs);
        FILE* file = fopen(CNF_FILE,"w");
        if(file!=NULL)
            fprintf(file,"%d %s\n",NumOfCur,interfaceName);
        fclose(file);
        sendReload(SIGUSR1);
    }
    else if(strcmp(argv[1],"--help") == 0 )
    {
        printf( "   ********                                                         \n");
        printf( "   * Help *                                                         \n");
        printf( "   ********                                                         \n");
        printf( "                                                                    \n");
        printf( "   start (packets are being sniffed from now on from default iface(%s)\n",interfaceName);
        printf( "                                                                    \n");
        printf( "   stop (packets are not sniffed)\n");
        printf( "                                                                    \n");
        printf( "   show [ip] count (print number of packets received from ip address)\n");
        printf( "                                                                    \n");
        printf( "   select iface [iface] (select interface for sniffing )\n");
        printf( "                                                                    \n");
        printf( "   stat [iface] show all collected statistics for particular interface, if iface omitted\n");
        printf( "   - for all interfaces.\n");
        printf( "                                                                    \n");
        printf( "   --help (show this information)\n");
        printf( "\n\n");
    }
}
