#include "PTree.h"

void printBinaryTree(PTree *p, int i) {
    if (p == NULL) {

        return;
    }
    memset(&source, 0, sizeof(source));
    source.sin_addr.s_addr = p->addr;
    printf("|-Source IP : %s  -- Number of packets received : %lld\n",inet_ntoa(source.sin_addr), p->num[i]);
    if(p->left != NULL)
    {
        printBinaryTree(p->left,i);
    }
    if(p->right != NULL)
    {
        printBinaryTree(p->right,i);
    }

}

PTree *search(uint32_t addr, PTree *leaf)
{
  if( leaf != 0 )
  {
      if(addr == leaf->addr)
      {
          return leaf;
      }
      else if(addr < leaf->addr)
      {
          return search(addr, leaf->left);
      }
      else
      {
          return search(addr, leaf->right);
      }
  }
  else return 0;
}

void insert(u_int32_t key, long long n, int i, PTree **leaf)
{
    if( *leaf == 0 )
        {
            *leaf = (PTree*) malloc( sizeof( PTree ) );
            (*leaf)->addr = key;
            (*leaf)->num = (long long*) malloc(NumOfIface*sizeof(long long));
            for (int k = 0; k < NumOfIface; k++)
                (*leaf)->num[k] = 0;
            (*leaf)->num[i] = n;
            /* initialize the children to null */
            (*leaf)->left = 0;
            (*leaf)->right = 0;
        }
        else if(key < (*leaf)->addr)
        {
            insert( key,n,i, &(*leaf)->left );
        }
        else if(key > (*leaf)->addr)
        {
            insert( key,n,i, &(*leaf)->right );
        }
    else if (key == (*leaf)->addr)
    {
        (*leaf)->num[i]+=n;
    }
}

void writeBinaryTree(PTree **p, FILE* customers) {
    if (*p == NULL) {
        return;
    }
    fwrite(&((*p)->addr),sizeof (uint32_t), 1, customers);
    fwrite(&NumOfIface,sizeof (int), 1, customers);
    fwrite((*p)->num,sizeof (long long), NumOfIface, customers);
    if((*p)->left != NULL)
    {
        writeBinaryTree(&(*p)->left,customers);
    }
    if((*p)->right != NULL)
    {
        writeBinaryTree(&(*p)->right,customers);
    }

}

void readBinaryTree(PTree **p, FILE* customers) {
  uint32_t addr ;
  int n;
  long long* x;
  while(fread(&addr,sizeof (uint32_t), 1, customers) == 1) {
      fread(&n,sizeof (int), 1, customers);
      x =(long long*) malloc(n*sizeof(long long));
      fread(x,sizeof (long long), n, customers);
      for (int i = 0; i < n; i++)
      {
        insert(addr,x[i],i, p);
      }
      free(x);
  }

}

void destroy_tree(PTree *leaf)
{
  if( leaf != NULL )
  {
      destroy_tree(leaf->left);
      destroy_tree(leaf->right);
      free(leaf->num);
      free( leaf );
  }
}
