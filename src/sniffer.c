#include<stdio.h> //For standard things
#include<stdlib.h>    //malloc
#include<string.h>    //memset
#include<netinet/ip_icmp.h>   //Provides declarations for icmp header
#include<netinet/udp.h>   //Provides declarations for udp header
#include<netinet/tcp.h>   //Provides declarations for tcp header
#include<netinet/ip.h>    //Provides declarations for ip header
#include<sys/socket.h>
#include<arpa/inet.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<linux/fs.h>
#include<sys/signal.h>

#include<pthread.h>

#include"sniffer.h"
#include"PTree.h"

extern int NumOfCur;
extern PTree* readTree;

void ProcessPacket(unsigned char* buffer)
{
    //Get the IP Header part of this packet
//    printf("[DAEMON] packet\n");
    struct iphdr *iph = (struct iphdr*)buffer;
    switch (iph->protocol) //Check the Protocol and do accordingly...
    {
        case 6:  //TCP Protocol
            print_ip_header(buffer );
            break;

        case 17: //UDP Protocol
            print_ip_header(buffer );
            break;
    }
    //printf("TCP : %d   UDP : %d   ICMP : %d   IGMP : %d   Others : %d   Total : %d\r",tcp,udp,icmp,igmp,others,total);
}
#define DAT_FILE "/var/lib/my_daemon.dat"

void print_ip_header(unsigned char* Buffer)
{
    struct iphdr *iph = (struct iphdr *)Buffer;
//    fprintf(logfile,"   |-Source IP        : %d    ----- %d\n",iph->saddr, 1);
//    PTree* z = 0;
//    z= search(iph->saddr,root);
//    if (z == 0)
    static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_lock(&mutex);
    insert(iph->saddr,1LL, NumOfCur, &readTree);
//    printf("%d\n",NumOfCur);
//    FILE* customers = fopen(DAT_FILE,"w");
//    writeBinaryTree(&readTree, customers);
//    fclose(customers);
    pthread_mutex_unlock(&mutex);
//    else
//        z->num++;

}
