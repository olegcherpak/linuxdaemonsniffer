#include "daemon.h"
#include "sniffer.h"
void sendReload(int sig)
{
    pid_t pid;
    FILE* pidfile = fopen(PID_FILE, "r");
    if (pidfile != NULL)
    {
        fscanf(pidfile,"%u",&pid);
        fclose(pidfile);
        kill(pid,sig);
    }
    else
    {
        printf("No working Daemon\n");
    }
}


void* startDeamon(void* arg)
{

        int data_size;
        socklen_t saddr_size;
        struct sockaddr saddr;
    //    struct in_addr in;
        int thread_oldstate;
        char interfaceName[256];
        strncpy(interfaceName,(char*) arg,256);
        unsigned char *buffer = (unsigned char *)malloc(65536); //Its Big!
        printf("[DAEMON] thread\n\n");
//        logfile=fopen("log.txt","w");
//        if(logfile==NULL) printf("Unable to create file.");
//        printf("Starting...\n");
        //Create a raw socket that shall sniff
        sock_raw = socket(AF_INET , SOCK_RAW , IPPROTO_TCP);
        if(sock_raw < 0)
        {
            WriteLog("Socket Error\n");

        }
        LoadConfig(interfaceName);
    //    char *opt;
    //    opt = "eth1";
    //    setsockopt(sock_raw, SOL_SOCKET, SO_BINDTODEVICE, opt, 4);
//        i = 0;
        while(1)
        {
            saddr_size = sizeof saddr;
            //Receive a packet
            data_size = recvfrom(sock_raw , buffer , 65536 , 0 , &saddr , &saddr_size);
            if(data_size <0 )
            {
                WriteLog("Recvfrom error , failed to get packets\n");
                exit(EXIT_FAILURE);
            }
            //Now process the packet
//            int ret;
            pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,&thread_oldstate);
            ProcessPacket(buffer );
            pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,&thread_oldstate);
        }
}

void WriteLog(char* Msg, ...)
{
    // тут должен быть код который пишет данные в лог
    FILE* log = fopen(LOG_FILE, "a");
    if (log == NULL)
        exit(EXIT_FAILURE);

    fprintf(log,Msg);
    fclose(log);
}

// функция загрузки конфига
int LoadConfig(char* interfaceName)
{

    if(setsockopt(sock_raw, SOL_SOCKET, SO_BINDTODEVICE, interfaceName, sizeof(interfaceName)) )
        WriteLog("LOAD ERROR\n");
    return 1;
}

// функция которая загрузит конфиг заново
// и внесет нужные поправки в работу
int ReloadConfig()
{
    FILE* file = fopen(CNF_FILE,"r");
    char interfaceName[256];
    fscanf(file, "%d %s\n", &NumOfCur, interfaceName);
    fclose(file);

//    memset(interfaceName, '\0', sizeof(interfaceName));
//    strncpy(interfaceName,buf,sizeof(interfaceName)- 1);
//    interfaceName[sizeof(interfaceName)- 1]='\0';
    return setsockopt(sock_raw, SOL_SOCKET, SO_BINDTODEVICE, interfaceName, sizeof(interfaceName));
}

// функция для остановки потоков и освобождения ресурсов
void DestroyWorkThread()
{
    // тут должен быть код который остановит все потоки и
    // корректно освободит ресурсы
    pthread_cancel(thread);
    close(sock_raw);
    FILE* customers = fopen(DAT_FILE,"w");
    writeBinaryTree(&readTree, customers);
    fclose(customers);
    destroy_tree(readTree);
}

// функция которая инициализирует рабочие потоки
int InitWorkThread(char* interfaceName)
{
    // код функции
    FILE* customers2 = fopen(DAT_FILE,"r");
    if (customers2 != NULL){
        readBinaryTree(&readTree, customers2);
        fclose(customers2);
    }
    int ret;
    ret = pthread_create(&thread,NULL,startDeamon,interfaceName);
    return ret;
}

// функция обработки сигналов
static void signal_error(int sig, siginfo_t *si, void *ptr)
{
    // запишем в лог что за сигнал пришел
    WriteLog("[DAEMON] Signal: %s, Addr: 0x%0.16X\n", strsignal(sig), si->si_addr);
    WriteLog("[DAEMON] Stopped\n");
    // остановим все рабочие потоки и корректно закроем всё что надо
    DestroyWorkThread();
    // завершим процесс с кодом требующим перезапуска
    exit(CHILD_NEED_WORK);
}


// функция установки максимального кол-во дескрипторов которое может быть открыто
int SetFdLimit(int MaxFd)
{
    struct rlimit lim;
    int           status;

    // зададим текущий лимит на кол-во открытых дискриптеров
    lim.rlim_cur = MaxFd;
    // зададим максимальный лимит на кол-во открытых дискриптеров
    lim.rlim_max = MaxFd;

    // установим указанное кол-во
    status = setrlimit(RLIMIT_NOFILE, &lim);

    return status;
}

void SetPidFile(char* Filename)
{
    FILE* f;

    f = fopen(Filename, "w+");
    if (f)
    {
            fprintf(f, "%u", getpid());
            fclose(f);
    }
}



int MonitorProc(char* interfaceName)
{
    struct sigaction sigact;
    sigset_t         sigset;
    int              signo;
    int              status;
    // сигналы об ошибках в программе будут обрататывать более тщательно
    // указываем что хотим получать расширенную информацию об ошибках
    sigact.sa_flags = SA_SIGINFO;
    // задаем функцию обработчик сигналов
    sigact.sa_sigaction = signal_error;
    sigemptyset(&sigact.sa_mask);
    // установим наш обработчик на сигналы
    sigaction(SIGFPE, &sigact, 0); // ошибка FPU
    sigaction(SIGILL, &sigact, 0); // ошибочная инструкция
    sigaction(SIGSEGV, &sigact, 0); // ошибка доступа к памяти
    sigaction(SIGBUS, &sigact, 0); // ошибка шины, при обращении к физической памяти
    sigemptyset(&sigset);
    // блокируем сигналы которые будем ожидать
    // сигнал остановки процесса пользователем
    sigaddset(&sigset, SIGQUIT);
    // сигнал для остановки процесса пользователем с терминала
    sigaddset(&sigset, SIGINT);
    // сигнал запроса завершения процесса
    sigaddset(&sigset, SIGTERM);
    // пользовательский сигнал который мы будем использовать для обновления конфига
    sigaddset(&sigset, SIGUSR1);
    sigaddset(&sigset, SIGUSR2);
    sigprocmask(SIG_BLOCK, &sigset, NULL);
    // Установим максимальное кол-во дискрипторов которое можно открыть
    SetFdLimit(FD_LIMIT);
    // запишем в лог, что наш демон стартовал
    SetPidFile(PID_FILE);
    WriteLog("[DAEMON] Started\n");
    printf("[DAEMON] Started\n\n");
    // запускаем все рабочие потоки
    status = InitWorkThread(interfaceName);
    if (!status)
    {
            // цикл ожижания сообщений
            for (;;)
            {


                    // ждем указанных сообщений
                    sigwait(&sigset, &signo);

                    // если то сообщение обновления конфига
                    if (signo == SIGUSR1)
                    {
                            // обновим конфиг
                            status = ReloadConfig();
                            if (status != 0)
                            {
                                    WriteLog("[DAEMON] Reload config failed\n");
                            }
                            else
                            {
                                    WriteLog("[DAEMON] Reload config OK\n");
                            }
                    }
                    else if (signo == SIGUSR2)
                    {
                        FILE* customers = fopen(DAT_FILE,"w");
                        writeBinaryTree(&readTree, customers);
                        fclose(customers);
                        WriteLog("WROTE\n");
//                        fprintBinaryTree(readTree,NumOfCur);
                    }
                    else // если какой-либо другой сигнал, то выйдим из цикла
                    {
                            break;
                    }
            }
            // остановим все рабочеи потоки и корректно закроем всё что надо
            DestroyWorkThread();
    }
    else
    {
            WriteLog("[DAEMON] Create work thread failed\n");
    }
    WriteLog("[DAEMON] Stopped\n");
    // удалим файл с PID'ом
    unlink(PID_FILE);

    return status;
}
