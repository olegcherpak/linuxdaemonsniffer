#
# TODO: Move `libmongoclient.a` to /usr/local/lib so this can work on production servers
#
 
CC := gcc # This is the main compiler
# CC := clang --analyze # and comment out the linker last line for sanity
SRCDIR := src
BUILDDIR := bin
TARGET := my_daemon

LOG_FILE      = /var/log/my_daemon.log
CNF_FILE      = /etc/my_daemon.conf
DAT_FILE      = /var/lib/my_daemon.dat
CREATE_FILE   = touch
DEL_FILE      = rm -f

SRCEXT := c
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

# -m64 -pipe -g -Wall -W -pthread

CFLAGS := -g -w -m64 -pipe
LIB := -pthread 
INC := -I include

$(TARGET): $(OBJECTS)
	@echo " Linking..."
	@echo " $(CC) $^ -o $(TARGET) $(LIB)"; $(CC) $^ -o $(TARGET) $(LIB)
	$(CREATE_FILE) $(LOG_FILE)
	$(CREATE_FILE) $(DAT_FILE)
	

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

clean:
	@echo " Cleaning..."; 
	@echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)
	$(DEL_FILE) $(LOG_FILE) 
	$(DEL_FILE) $(DAT_FILE)
	$(DEL_FILE) $(CNF_FILE) 

.PHONY: clean