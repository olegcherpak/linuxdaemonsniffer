#ifndef PTREE_H
#define PTREE_H

#include<stdio.h> //For standard things
#include<stdlib.h>    //malloc
#include<string.h>    //memset
#include<netinet/ip_icmp.h>   //Provides declarations for icmp header
#include<netinet/udp.h>   //Provides declarations for udp header
#include<netinet/tcp.h>   //Provides declarations for tcp header
#include<netinet/ip.h>    //Provides declarations for ip header
#include<sys/socket.h>
#include<arpa/inet.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<linux/fs.h>
#include<sys/signal.h>

typedef struct tree
{
    u_int32_t addr;
    long long* num;
    struct tree* left;
    struct tree* right;
} PTree;

extern struct sockaddr_in source,dest;
extern int NumOfIface;

//extern PTree* readTree;


void writeBinaryTree(PTree**, FILE*);
PTree *search(u_int32_t, PTree*);
void insert(u_int32_t, long long, int , PTree **);
void readBinaryTree( PTree **, FILE*);
void destroy_tree(PTree *);
void printBinaryTree(PTree *, int);
//void fprintBinaryTree(PTree *, int );

#endif
