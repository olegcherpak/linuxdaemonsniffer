#ifndef DAEMON_H
#define DAEMON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <execinfo.h>
#include <unistd.h>
#include <errno.h>
#include <wait.h>
#include <sys/time.h>
#include <sys/resource.h>

// лимит для установки максимально кол-во открытых дискрипторов
#define FD_LIMIT			1024*10

// константы для кодов завершения процесса
#define CHILD_NEED_WORK			1
#define CHILD_NEED_TERMINATE	2

#define PID_FILE "/var/run/my_daemon.pid"
#define LOG_FILE "/var/log/my_daemon.log"
#define CNF_FILE "/etc/my_daemon.conf"
#define DAT_FILE "/var/lib/my_daemon.dat"

#include<pthread.h>
#define DELIM "="
#include <string.h>
#include<signal.h>
#include <sys/types.h>
#include <ifaddrs.h>

#include "PTree.h"
extern int sock_raw;
extern pthread_t thread;
extern PTree* readTree;
extern int NumOfCur;

void* startDeamon(void*);
void WriteLog(char* , ...);
int LoadConfig(char*);
int ReloadConfig();
void DestroyWorkThread();
int InitWorkThread(char*);
static void signal_error(int , siginfo_t *, void *);
int SetFdLimit(int );
void SetPidFile(char* );
int MonitorProc(char*);
void sendReload(int);

#endif
